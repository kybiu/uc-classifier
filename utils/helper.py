import pandas as pd
from spelling_correction.heuristic_correction import *
import logging
import time
import re
import unicodedata
import os

logging.root.setLevel(logging.NOTSET)
logging.basicConfig(
    level=logging.NOTSET,
    format='%(asctime)s %(module)s - %(funcName)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)
logger = logging.getLogger(__name__)

with open("obj_type", "r", encoding="utf-8") as obj_type_file:
    lines = obj_type_file.readlines()
    objtype_list = [x.strip().replace("_", " ") for x in lines]

with open("brand", "r", encoding="utf-8") as obj_type_file:
    lines = obj_type_file.readlines()
    brand_list = [x.strip() for x in lines]


def remove_col_str(customer_messages):
    start_time = time.time()
    logger.info("Remove col string")

    # remove a portion of string in a dataframe column - col_1
    customer_messages = [str(x).replace('\n', ' ') for x in customer_messages]
    # remove all the characters after &# (including &#) for column - col_1
    customer_messages = [str(x).replace(' &#.*', ' ') for x in customer_messages]

    logger.info(str(time.time() - start_time))
    return customer_messages


def remove_col_white_space(customer_messages):
    start_time = time.time()
    logger.info("Remove col white space")

    # remove white space at the beginning of string
    customer_messages = [str(x).strip() for x in customer_messages]

    logger.info(str(time.time() - start_time))
    return customer_messages


def correction_message(customer_messages):
    start_time = time.time()
    logger.info("Correction message")
    filter_text = ["facebook", "started", "/confirm_attribute_value", '/confirm_object_type', '/connect_employee',
                   '/deny_object_type', '/agree', '/disagree', '/find_product_by_category',
                   '/greet', '/query_knowledge_base', '/restart', '/show_shop_address', '/show_shop_contact',
                   '/start_conversation', 'remind_ask_rephrase', "/buy_product", "Get Started"]

    correct_messages = []
    for message in customer_messages:
        if any(x in message for x in filter_text):
            correct_messages.append(message)
        else:
            message = do_correction(message)
            correct_messages.append(message)
        # if all(x not in message for x in filter_text) and message not in ["/buy_product", "Get Started"]:
        #     message = do_correction(str(message))
        correct_messages.append(message)

    logger.info(str(time.time() - start_time))
    return correct_messages


def remove_special_characters(customer_messages):
    start_time = time.time()
    logger.info("Remove special character")

    customer_messages = [re.sub('\}|\{|\]|\[|\;|\.|\,|\.|\:|\!|\@|\#|\$|\^|\&|\(|\)|\<|\>|\?|\"|\'', ' ', str(x)) for x
                         in customer_messages]
    # customer_messages = [x for x in customer_messages if x != ' ']

    logger.info(str(time.time() - start_time))
    return customer_messages


def deEmojify(customer_messages):
    start_time = time.time()
    logger.info("de-emojify")

    regrex_pattern = re.compile(pattern="["
                                        u"\U0001F600-\U0001F64F"  # emoticons
                                        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                                        u"\U0001F680-\U0001F6FF"  # transport & map symbols
                                        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                                        "]+", flags=re.UNICODE)
    new_customer_message = []
    for message in customer_messages:
        try:
            deemojy_message = regrex_pattern.sub(r'', message)
            new_customer_message.append(deemojy_message)
        except:
            new_customer_message.append(message)

    logger.info(str(time.time() - start_time))
    return new_customer_message


def do_clean_message(customer_messages):
    start_time = time.time()
    logger.info("Export clean customer message")
    clean_customer_messages = remove_col_str(customer_messages)
    clean_customer_messages = deEmojify(clean_customer_messages)
    clean_customer_messages = remove_special_characters(clean_customer_messages)
    clean_customer_messages = correction_message(clean_customer_messages)
    clean_customer_messages = remove_col_white_space(clean_customer_messages)
    df = pd.DataFrame({"customer_message": customer_messages, "clean_customer_message": clean_customer_messages})
    df.to_csv("data/customer_message/customer_messages.csv", index=False)
    logger.info(str(time.time() - start_time))
    return df
