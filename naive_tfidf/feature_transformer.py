from pyvi import ViTokenizer, ViPosTagger
from sklearn.base import TransformerMixin, BaseEstimator
from underthesea import word_tokenize

# class FeatureTransformer(BaseEstimator, TransformerMixin):
class FeatureTransformer():
    def __init__(self):
        self.tokenizer = None
        self.pos_tagger = None

    def fit(self, *_):
        return self

    def transform(self, X, y=None, **fit_params):
        result = X.apply(lambda text: ViTokenizer.tokenize(text))
        return result
